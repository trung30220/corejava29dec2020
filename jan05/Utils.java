package jan05;

import java.util.Scanner;

public class Utils {
    public static int nhapMotSo(String s) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(s);
        int a = scanner.nextInt();
        return a;
    }

    public static void checkTamGiac(int a, int b, int c) {
        if (a + b > c & a + c > b & b + c > a) {

            if (a * a + b * b == c * c) {
                System.out.println("tam giac vuong o C");
            } else if (a * a + c * c == b * b) {
                System.out.println("tam giac vuong o B");
            } else if (b * b + c * c == a * a) {
                System.out.println("tam giac vuong o A");
            } else {
                System.out.println("tam giac ABC khong vuong");
            }
            if (a == b & a != c) {
                System.out.println("tam giac ABC can o C");
            } else if (a == c & a != b) {
                System.out.println("tam giac ABC can o B");
            } else if (c == b & c != a) {
                System.out.println("tam giac ABC can o A");
            } else {
                System.out.println("tam giac ABC khong can");
            }
            if (a == b & a == c) {
                System.out.println("tam giac ABC deu");
            }
        } else {
            System.out.println("khong ton tai tam giac");
        }
    }
}
