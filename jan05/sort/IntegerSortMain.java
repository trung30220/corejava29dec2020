package jan05.sort;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class IntegerSortMain {
    public static void main(String[] args) {
        List<Integer> listInteger = new ArrayList<>();
        listInteger.add(10);
        listInteger.add(30);
        listInteger.add(1);
        listInteger.add(30);
        System.out.println(listInteger);
        System.out.println(listInteger.size());

        List<Integer> li2 = new ArrayList<>();

        for (int i = 0;i < listInteger.size();i ++) {
            int r = listInteger.get(i);
            System.out.println("r = " + r);
            li2.add(r*2);
        }

        System.out.println(li2);
    }
}
