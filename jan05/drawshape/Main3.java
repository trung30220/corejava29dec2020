package jan05.drawshape;

import java.util.Scanner;

public class Main3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Nhap chieu rong:");
        int r = scanner.nextInt();

        System.out.println("Nhap chieu cao:");
        int c = scanner.nextInt();

        for (int i = 0;i < c;i++) {
            if (i % 2 == 0) {
                func2(r/2, " |");
            } else {
                func2(r, "*");
            }
            System.out.println();
        }
    }

    public static void func2(int x, String y) {
        for (int j = 0;j < x;j++) {
            System.out.print(y);
        }
    }
}
