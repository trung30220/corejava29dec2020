package jan05.drawshape;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Nhap chieu rong:");
        int r = scanner.nextInt();

        System.out.println("Nhap chieu cao:");
        int c = scanner.nextInt();

        for (int i = 0;i < c;i++) {
            if (i == 0 || i == c-1) {
                for (int j = 0;j < r;j++) {
                    System.out.print("x");
                }
            } else {
                System.out.print("x");
                for (int j = 1;j < r-1;j++) {
                    System.out.print(" ");
                }
                System.out.print("x");
            }
            System.out.println();
        }
    }
}
