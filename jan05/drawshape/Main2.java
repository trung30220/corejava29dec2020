package jan05.drawshape;

import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Nhap chieu rong:");
        int r = scanner.nextInt();

        System.out.println("Nhap chieu cao:");
        int c = scanner.nextInt();

        for (int i = 0;i < c;i++) {
            if (i % 2 == 0) {
                for (int j = 0;j < r/2;j++) {
                    System.out.print(" ");
                    System.out.print("*");
                }
            } else {
                for (int j = 0;j < r;j++) {
                    System.out.print("*");
                }
            }
            System.out.println();
        }
    }
}
