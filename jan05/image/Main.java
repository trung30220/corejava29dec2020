package jan05.image;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Image[] i2 = new Image[1024];
        Image[][] i1 = new Image[1024][800];
        Image[][][] i3 = new Image[1024][800][444];

        int[] sn = new int[10];
        sn[0] = 10;
        sn[1] = 20;
        sn[5] = 13444;
        Arrays.sort(sn);

        for (int i = 0;i < sn.length;i++) {
            System.out.println(sn[i]);
        }
    }
}
